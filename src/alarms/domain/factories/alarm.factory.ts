import { Injectable } from '@nestjs/common';
import { randomUUID } from 'crypto';
import {
  AlarmSeverity,
  Severity as SeverityType,
} from '../value-objects/alarm-severity';
import { Alarm } from '../alarm';
import { AlarmItem } from '../alarm-item';

@Injectable()
export class AlarmFactory {
  public create(
    name: string,
    severity: string,
    triggeredAt: Date,
    items: Array<{ name: string; type: string }>,
  ): Alarm {
    const alarmID = randomUUID();
    const alarmSeverity = new AlarmSeverity(severity as SeverityType);
    const alarm = new Alarm(alarmID);

    alarm.name = name;
    alarm.severity = alarmSeverity;
    alarm.triggeredAt = triggeredAt;
    items
      .map((item) => new AlarmItem(randomUUID(), item.name, item.type))
      .forEach((item) => alarm.addAlarmItem(item));

    return alarm;
  }
}
