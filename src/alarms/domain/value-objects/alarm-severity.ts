export enum Severity {
  CRITIAL = 'critical',
  HIGH = 'high',
  MEDIUM = 'medium',
  LOW = 'low',
}

export class AlarmSeverity {
  constructor(readonly value: Severity) {}

  public equals(severity: AlarmSeverity): boolean {
    return this.value === severity.value;
  }
}
