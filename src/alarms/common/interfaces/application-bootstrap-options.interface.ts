import { DriverType } from 'src/alarms/infrastructure/alarms-infrastructure.module';

export interface ApplicationBootstrapOptions {
  driver: DriverType;
}
