import { Module } from '@nestjs/common';
import { InmemoryAlarmPersistanceModule } from './persistance/in-memories/inmemory-persistance.module';
import { OrmAlarmPersistanceModule } from './persistance/orm/orm-persistance.module';

// enum from DependencyType
export enum DriverType {
  ORM = 'orm',
  IN_MEMORY = 'in_memory',
}

@Module({})
export class AlarmsInfrastructureModule {
  static use(driver: DriverType) {
    const persistanceModule =
      driver === DriverType.ORM
        ? OrmAlarmPersistanceModule
        : InmemoryAlarmPersistanceModule;

    return {
      module: AlarmsInfrastructureModule,
      imports: [persistanceModule],
      exports: [persistanceModule],
    };
  }
}
