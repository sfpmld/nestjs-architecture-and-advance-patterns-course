import { Severity } from 'src/alarms/domain/value-objects/alarm-severity';
import { PrimaryColumn, Column, Entity, OneToMany } from 'typeorm';
import { AlarmItemEntity } from './alarm-item.entity';

@Entity({ name: 'alarms' })
export class AlarmEntity {
  @PrimaryColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  severity: Severity;

  @Column()
  triggeredAt: Date;

  @Column()
  isAcknowledged: boolean;

  @OneToMany(() => AlarmItemEntity, (item) => item.alarm, { cascade: true })
  items: AlarmItemEntity[];
}
