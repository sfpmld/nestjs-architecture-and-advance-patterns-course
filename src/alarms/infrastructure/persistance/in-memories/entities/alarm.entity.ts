import { Severity } from 'src/alarms/domain/value-objects/alarm-severity';
import { AlarmItemEntity } from './alarm-item.entity';

export class AlarmEntity {
  id: string;

  name: string;

  severity: Severity;

  triggeredAt: Date;

  isAcknowledged: boolean;

  items: Array<AlarmItemEntity>;
}
