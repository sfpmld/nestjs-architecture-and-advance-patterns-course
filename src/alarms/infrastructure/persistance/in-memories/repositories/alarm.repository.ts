import { CreateAlarmRepository } from 'src/alarms/application/ports/create-alarm.repository';
import { Alarm } from 'src/alarms/domain/alarm';
import { AlarmMapper } from 'src/alarms/common/mappers/alarm.mapper';
import { AlarmEntity } from '../entities/alarm.entity';
import { FindAlarmsRepository } from 'src/alarms/application/ports/find-alarms.repository';
import { UpsertMaterializedAlarmRepository } from 'src/alarms/application/ports/upsert-materialized-alarm.repository';
import { AlarmReadModel } from 'src/alarms/domain/read-models/alarm.read-model';

export class InMemoryAlarmRepository
  implements
    CreateAlarmRepository,
    FindAlarmsRepository,
    UpsertMaterializedAlarmRepository
{
  private readonly alarms = new Map<string, AlarmEntity>();
  private readonly materializedAlarmViews = new Map<string, AlarmReadModel>();

  async findAll(): Promise<AlarmReadModel[]> {
    return Array.from(this.materializedAlarmViews.values());
  }

  async save(alarm: Alarm): Promise<Alarm> {
    // const entityToPersist = AlarmMapper.toPersistence(alarm);
    // this.alarms.set(entityToPersist.id, entityToPersist);
    // return AlarmMapper.toDomain(this.alarms.get(entityToPersist.id));
    const persistanceModel = AlarmMapper.toPersistence(alarm);
    this.alarms.set(persistanceModel.id, persistanceModel);

    const newEntity = this.alarms.get(persistanceModel.id);
    return AlarmMapper.toDomain(newEntity);
  }

  upsert(
    alarm: Pick<AlarmReadModel, 'id'> & Partial<AlarmReadModel>,
  ): Promise<void> {
    if (this.materializedAlarmViews.has(alarm.id)) {
      this.materializedAlarmViews.set(alarm.id, {
        ...this.materializedAlarmViews.get(alarm.id),
        ...alarm,
      });
    }
    return;
  }
}
