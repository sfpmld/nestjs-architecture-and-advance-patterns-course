import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DriverType } from './alarms/infrastructure/alarms-infrastructure.module';

async function bootstrap() {
  const app = await NestFactory.create(
    AppModule.register({ driver: DriverType.ORM }),
  );
  await app.listen(3000);
}
bootstrap();
